FROM python:latest
MAINTAINER Jonathan Ferretti

RUN apt-get update -y && apt-get install -y \
    python-dev \
    python-pip \
    curl \
    nginx \
    wget

RUN mkdir -p /opt/panw_app_framework/ && \
    mkdir -p /var/log/panw_app_framework && \
    mkdir -p /var/log/supervisord 

WORKDIR /opt/panw_app_framework

COPY requirements.txt /opt/panw_app_framework/requirements.txt
RUN pip install -U pip && \
    pip install -r requirements.txt && \
    pip2 install supervisor

COPY app /opt/panw_app_framework/app
COPY run.py /opt/panw_app_framework/run.py
COPY res /opt/panw_app_framework/res
COPY config.py /opt/panw_app_framework/config.py


RUN rm /etc/nginx/nginx.conf && ln -s /opt/panw_app_framework/res/etc/nginx/nginx.conf  /etc/nginx/nginx.conf && \
    rm -rf /etc/nginx/conf.d && ln -s /opt/panw_app_framework/res/etc/nginx/conf.d /etc/nginx/conf.d && \
    ln -s /opt/panw_app_framework/res/etc/supervisord.conf /etc/supervisord.conf 


CMD supervisord -c /etc/supervisord.conf
