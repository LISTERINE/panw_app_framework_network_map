from flask import Blueprint, current_app, jsonify, request
from flask_socketio import emit
from app import socketio

from collections import defaultdict

panw = Blueprint("panw_api", __name__, url_prefix="/api/v1/panw")


def get_traffic(count=10, start_time=0, end_time=1609459200, max_wait=5000):
    """ Pull traffic data from panw log service."""
    d = {"query": "select * from panw.traffic limit {}".format(count),
         "startTime": start_time,  # 1970
         "endTime": end_time,  # 2021
         "maxWaitTime": max_wait}
    q = current_app.logging_service.query(json=d)
    return q.json()


def get_threats(start_time=0, end_time=1609459200, max_wait=5000):
    """ Get threat logs from panw log service."""
    d = {"query": "select * from panw.threat",
         "startTime": start_time,  # 1970
         "endTime": end_time,  # 2021
         "maxWaitTime": max_wait}
    q = current_app.logging_service.query(json=d)
    return q.json()


def build_traffic_map(traffic_args, threat_args):
    """ Create the json structures expected by vis.js."""
    # XXX Joins and pagination would make this much simpler and less memory heavy on the client
    raw_traffic = get_traffic(**traffic_args)['result']['esResult']['hits']['hits']
    raw_threats = get_threats(**threat_args)['result']['esResult']['hits']['hits']
    threat_hosts = set()
    for threat in raw_threats:
        threat_hosts.add(threat["_source"]["src"])
        threat_hosts.add(threat["_source"]["dst"])
    connections = defaultdict(set)
    hosts = defaultdict(dict)
    connection_edges = []
    for conn_data in raw_traffic:
        src = conn_data["_source"]["natsrc"]
        dst = conn_data["_source"]["natdst"]
        hosts[src]["bad"] = bool(src in threat_hosts)
        hosts[dst]["bad"] = bool(dst in threat_hosts)
        connections[src].add(dst)
    host_nodes = [{"id": ip, "label": ip, "color": "red" if data.get("bad") else "blue"} for ip, data in hosts.items()]
    for src, dsts in connections.items():
        for dst in dsts:
            connection_edges.append({"from": src, "to": dst, "arrows": "to"})
    return host_nodes, connection_edges


@panw.route("/traffic_map", methods=["GET"])
def get_traffic_route():
    req = request.args
    params = ["count", "start_time", "end_time", "max_wait"]
    args = {param: req.get(param) for param in params if req.get(param)}
    nodes, edges = build_traffic_map(args, {})
    return jsonify(nodes=nodes, edges=edges)


@socketio.on('get_traffic')
def get_socket_traffic(data=None):
    data = data or {}
    nodes, edges = build_traffic_map(data, {})
    emit("traffic", {"nodes": nodes, "edges": edges})
