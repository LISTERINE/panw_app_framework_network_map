function fetch_traffic_data(){
	$.ajax({
	  dataType: "json",
	  url: "/api/v1/panw/traffic_map?count=2000",
	  success: write_data_to_graph
	});
}


function write_data_to_graph(data, status, jqXHR){
	// create a network
	var container = document.getElementById('mynetwork');
	var data = {
	  nodes: data.nodes,
	  edges: data.edges
	};
	var options = {};
	var network = new vis.Network(container, data, options);
}
fetch_traffic_data();


var socket = io.connect("http://" + document.domain + ":" + location.port);
socket.on("traffic", function(msg){
	console.log("got: "+msg);
	write_data_to_graph(msg, null, null);
});

setInterval(function(){ socket.emit("get_traffic", {"count": 2000}); }, 15000);
