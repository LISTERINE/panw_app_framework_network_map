from flask import Blueprint, render_template


panw = Blueprint("panw_view", __name__, url_prefix="/panw")


@panw.route("/traffic")
def traffic():
    return render_template("traffic.html")
