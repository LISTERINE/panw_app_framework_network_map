dockerbuild:
	docker build --no-cache -t panw_app_framework:latest .

dockerbuildcache:
	docker build -t panw_app_framework:latest .

dockerrun:
	make dockerbuildcache && docker-compose up -d



dockerclean:
	docker-compose kill && docker-compose rm

dockerexec:
	docker exec -it $$(docker-compose ps -q |head -n 1) /bin/bash

letsencrypt:
	docker-compose exec flask certbot --nginx
	docker-compose restart
